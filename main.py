import psutil
import time

name_process = "GTA5.exe"


def main():

    for proc in psutil.process_iter(['pid', "name", "username"]):

        if proc.info['name'] == name_process:
            pid = psutil.Process(proc.info['pid'])
            pid.suspend()
            print("Start Suspense, wait 10 seconds ...")
            time.sleep(10)
            pid.resume()
            print("sessions solo ok")
            break


if __name__ == '__main__':
    main()
